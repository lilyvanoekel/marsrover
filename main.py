from rover.nasa import Nasa

class MainProgram:
    def __init__(self):
        self.nasa = Nasa();
    
    def requestPlateau(self):
        self.nasa.createPlateau(
            input('Plateau: ')
        )
    
    def requestRoverLanding(self):
        self.nasa.createRover(
            input('Rover{} Landing: '.format(self.currentRover))
        )
    
    def requestRoverInstructions(self):
        self.nasa.commandRover(
            self.currentRover - 1, 
            input('Rover{} Instructions: '.format(self.currentRover))
        )
        
    def tryForever(self, method):
        while(True):
            try:
                method();
                break
            except:
                print("Please check your input and try again, or press ctrl-c to quit")
    
    def run(self):
        self.tryForever(self.requestPlateau)
        
        self.currentRover = 1
        self.tryForever(self.requestRoverLanding)
        self.tryForever(self.requestRoverInstructions)
        
        self.currentRover = 2
        self.tryForever(self.requestRoverLanding)
        self.tryForever(self.requestRoverInstructions)
        
        print('Rover 1:{}'.format(self.nasa.rovers[0].getPosition()))
        print('Rover 2:{}'.format(self.nasa.rovers[1].getPosition()))

if __name__ == '__main__':
    MainProgram().run()

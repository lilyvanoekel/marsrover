class Rover:
    directions = ('N', 'E', 'S', 'W')
    
    def __init__(self, x, y, direction, plateau):
        if (x < 0):
            raise ValueError('x can\'t be smaller than 0')
        elif (y < 0):
            raise ValueError('y can\'t be smaller than 0')
        #elif (x > plateau.width - 1):
        #    raise ValueError('x has to be smaller than the width of the plateau')
        #elif (y > plateau.height -1):
        #    raise ValueError('y has to be smaller than the height of the plateau')
        
        try:
            self.directions.index(direction)
        except:
            raise ValueError('Invalid direction')
        
        self.x = x
        self.y = y
        self.direction = direction
        self.plateau = plateau
    
    def turnLeft(self):
        directionIndex = self.directions.index(self.direction)
        directionIndex = directionIndex - 1 if directionIndex > 0 else 3
        self.direction = self.directions[directionIndex]
    
    def turnRight(self):
        directionIndex = self.directions.index(self.direction)
        directionIndex = directionIndex + 1 if directionIndex < 3 else 0
        self.direction = self.directions[directionIndex]
    
    def move(self):
        if (self.direction == 'N'):
            #if (self.y >= self.plateau.height - 1):
            #    raise ValueError('Unable to move outside of north boundary of plateau')
            self.y += 1
        elif (self.direction == 'E'):
            #if (self.x >= self.plateau.width - 1):
            #    raise ValueError('Unable to move outside of east boundary of plateau')
            self.x += 1
        elif (self.direction == 'S'):
            if (not self.y):
                raise ValueError('Unable to move south of y coordinate 0')
            self.y -= 1
        else:
            if (not self.y):
                raise ValueError('Unable to move west of x coordinate 0')
            self.x -= 1
    
    def getPosition(self):
        return '{} {} {}'.format(self.x, self.y, self.direction)
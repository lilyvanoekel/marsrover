from rover.plateau import Plateau
from rover.rover import Rover

class Nasa:
    def __init__(self):
        self.rovers = []
        
    def createPlateau(self, input):
        input = input.split()
        
        self.plateau = Plateau(
            int(input[0]), 
            int(input[1])
        )
    
    def createRover(self, input):
        input = input.split()
        
        self.rovers.append(
            Rover(
                int(input[0]),
                int(input[1]),
                input[2],
                self.plateau
            )
        )
    
    def commandRover(self, index, command):
        for c in command:
            if (c == 'L'):
                self.rovers[index].turnLeft()
            elif (c == 'R'):
                self.rovers[index].turnRight()
            elif (c == 'M'):
                self.rovers[index].move()
            else:
                raise ValueError('Invalid command')
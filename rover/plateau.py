class Plateau:
    def __init__(self, width, height):
        if (width < 1):
            raise ValueError('Width has to be bigger than 0')
        elif (height < 1):
            raise ValueError('Height has to be bigger than 0')
        
        self.width = width
        self.height = height
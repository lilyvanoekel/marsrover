import unittest
from rover.nasa import Nasa

class TestNasa(unittest.TestCase):
    def test_nasa_constructor(self):
        nasa = Nasa()
        
    def test_nasa_create_plateau(self):
        nasa = Nasa()
        nasa.createPlateau('5 6')
        self.assertEqual(nasa.plateau.width, 5)
        self.assertEqual(nasa.plateau.height, 6)
        
    def test_nasa_create_rover(self):
        nasa = Nasa()
        nasa.createPlateau('5 6')
        nasa.createRover('1 2 N')
        self.assertEqual(nasa.rovers[0].getPosition(), '1 2 N')
    
    def test_nasa_process_commands(self):
        nasa = Nasa()
        nasa.createPlateau('5 5')
        nasa.createRover('1 2 N')
        nasa.commandRover(0, 'LMLMLMLMM')
        self.assertEqual(nasa.rovers[0].getPosition(), '1 3 N')
        
        nasa.createRover('3 3 E')
        nasa.commandRover(1, 'MMRMMRMRRM')
        self.assertEqual(nasa.rovers[1].getPosition(), '5 1 E')
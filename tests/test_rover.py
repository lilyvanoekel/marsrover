import unittest
from rover.rover import Rover
from rover.plateau import Plateau

class TestRover(unittest.TestCase):
    def create_rover(self, x=1, y=2, direction='E'):
        return Rover(x, y, direction, Plateau(5, 5))
    
    def test_rover_constructor(self):
        rover = self.create_rover()
        self.assertEqual(rover.x, 1)
        self.assertEqual(rover.y, 2)
        self.assertEqual(rover.direction, 'E')
    
    def test_rover_can_turn_left(self):
        rover = self.create_rover()
        expectedResults = ('N', 'W', 'S', 'E')
        
        for expectedDirection in expectedResults:
            rover.turnLeft()
            self.assertEqual(rover.direction, expectedDirection)
    
    def test_rover_can_turn_right(self):
        rover = self.create_rover()
        expectedResults = ('S', 'W', 'N', 'E')
        
        for expectedDirection in expectedResults:
            rover.turnRight()
            self.assertEqual(rover.direction, expectedDirection)    
    
    def test_rover_can_move_north(self):
        rover = self.create_rover(direction='N')
        
        rover.move();
        self.assertEqual(rover.y, 3)
        
        rover.move();
        self.assertEqual(rover.y, 4)
    
    def test_rover_can_move_east(self):
        rover = self.create_rover()
        rover.move();
        self.assertEqual(rover.x, 2)
    
    def test_rover_can_move_south(self):
        rover = self.create_rover(direction='S')
        rover.move();
        self.assertEqual(rover.y, 1)
        
    def test_rover_can_move_west(self):
        rover = self.create_rover(direction='W')
        rover.move();
        self.assertEqual(rover.x, 0)
    
    def test_rover_throws_exception_when_coordinates_negative(self):
        rover = self.create_rover(0, 0, 'S')
        self.assertRaises(ValueError, rover.move)
        
        roverWest = self.create_rover(0, 0, 'W')
        self.assertRaises(ValueError, roverWest.move)
        
        pass
    
    def test_rover_throws_exception_when_moving_off_plateau(self):
        #rover = self.create_rover(0, 4, 'N')
        #self.assertRaises(ValueError, rover.move)
        
        #roverWest = self.create_rover(4, 0, 'E')
        #self.assertRaises(ValueError, roverWest.move)
        
        pass
    
    def test_rover_throws_exception_invalid_values_constructor(self):
        #self.assertRaises(ValueError, self.create_rover, -1, 0, 'E')
        #self.assertRaises(ValueError, self.create_rover, 0, -1, 'E')
        self.assertRaises(ValueError, self.create_rover, 0, 0, 'Q')
        #self.assertRaises(ValueError, self.create_rover, 0, 5, 'E')
        #self.assertRaises(ValueError, self.create_rover, 5, 0, 'E')
    
    def test_rover_get_position(self):
        rover = self.create_rover(3, 4, 'W');
        self.assertEqual(rover.getPosition(), '3 4 W')
        
if __name__ == '__main__':
    unittest.main()
import unittest
from rover.plateau import Plateau

class TestPlateau(unittest.TestCase):
    def test_plateau_constructor(self):
        plateau = Plateau(6, 5)
        self.assertEqual(plateau.width, 6)
        self.assertEqual(plateau.height, 5)
    
    def test_plateau_invalid_values(self):
        self.assertRaises(ValueError, Plateau, -1, 5)
        self.assertRaises(ValueError, Plateau, 5, -4)
Lily's implementation of https://github.com/abdulg/Mars-Rover

To run the program please use: python main.py

To run tests please use: python -m unittest

Tested on Python 3.5.2